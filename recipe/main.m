//
//  main.m
//  recipe
//
//  Created by User on 2/8/16.
//  Copyright (c) 2016 denizburak. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
