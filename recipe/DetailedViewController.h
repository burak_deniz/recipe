//
//  DetailedViewController.h
//  recipe
//
//  Created by User on 2/8/16.
//  Copyright (c) 2016 denizburak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailedViewController : UIViewController

@property (nonatomic, strong) NSDictionary* infoDictionary;


@end
