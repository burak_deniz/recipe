//
//  AppDelegate.h
//  recipe
//
//  Created by User on 2/8/16.
//  Copyright (c) 2016 denizburak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
