//
//  ViewController.m
//  recipe
//
//  Created by User on 2/8/16.
//  Copyright (c) 2016 denizburak. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSString* path = [[NSBundle mainBundle]pathForResource:@"Prpperty List" ofType:@"plist"];
    Array = [[NSArray alloc] initWithContentsOfFile:path];
    
    Array = @[@"CheeseBurger", @"Mac N Cheese", @"Pasta", @"Brownie", @"Rice"];
  
    
    
    RecipeTable = [[UITableView alloc]
                   initWithFrame:self.view.bounds style: UITableViewStylePlain];
    RecipeTable.delegate=self;
    RecipeTable.dataSource= self;
    [self.view addSubview:RecipeTable];
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{return Array.count;}

-(UITableViewCell *)tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier:@"cell"];}
    
    NSDictionary* dictionary = Array[indexPath.row];
    NSString* stringToDisplay = dictionary[@"Title"];
    
    cell.textLabel.text = stringToDisplay;
    
    
    return cell;}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
     NSDictionary* dictionary = Array[indexPath.row];

DetailedViewController* dvc = [DetailedViewController new];
    
    dvc.infoDictionary = dictionary;
    
[self.navigationController pushViewController : dvc animated:YES];

}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
