//
//  DetailedViewController.m
//  recipe
//
//  Created by User on 2/8/16.
//  Copyright (c) 2016 denizburak. All rights reserved.
//

#import "DetailedViewController.h"

@interface DetailedViewController ()

@end

@implementation DetailedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    self.view.backgroundColor = [UIColor darkGrayColor];
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
    iv.image = [UIImage imageNamed:self.infoDictionary[@"Image"]];
    iv.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:iv];
    
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
